const fs = require("fs");
const _ = require("lodash");

const sampleFile = JSON.parse(fs.readFileSync("./claim-sample.json"));

const stripSegment = (claimId) => {
  if (claimId.length === 11) {
    return (claimRootId = claimId.slice(0, -2));
  }
  throw new Error("BLOW UP!");
};

finalClaim = sampleFile.reduce((result, nextClaim, currentIndex) => {
  let nextClaimRootId = stripSegment(nextClaim.claimId);

  // this will merge a legitimately duplicated bill when we dno't want it to.
  let existingClaim = result.find((it) => {
    return it.claimId === nextClaimRootId;
  });

  if (existingClaim) {
    existingClaim.serviceLines = [...existingClaim.serviceLines, ...nextClaim.serviceLines];
    existingClaim.total += nextClaim.total;
    existingClaim.ineligible += nextClaim.ineligible;
    existingClaim.discounted += nextClaim.discounted;
    existingClaim.ahpDeducted += nextClaim.ahpDeducted;
    existingClaim.copayAmount += nextClaim.copayAmount;
    existingClaim.outOfNetwork += nextClaim.outOfNetwork;
    existingClaim.coInsAmt += nextClaim.coInsAmt;
    existingClaim.overallMemberResponsibility += nextClaim.overallMemberResponsibility;
    existingClaim.medishareResponsibility += nextClaim.medishareResponsibility;
    // This is a little hacky.  Basically on the first pass when a new claim is added, the
    // roll up inherits the original claims status.  On subsequent roll up attempts however,
    // we need to update the roll up claims status if ANY of its child segments are still
    // pending
    if (nextClaim.processed === false) {
      existingClaim.processed = false;
      existingClaim.claimStatus = "Pending";
    }
  } else {
    result.push(nextClaim);
    result[0].claimId = nextClaimRootId; // rename claim to strip segment
  }
  return result;
}, []);

console.log("Starting...");
console.log(finalClaim);

// JUNK
// result.push({
//   claimId: nextClaimRootId,
//   total: nextClaim.total,
//   ineligible: nextClaim.ineligible,
//   discounted: nextClaim.discounted,
//   ahpDeducted: nextClaim.ahpDeducted,
//   copayAmount: nextClaim.copayAmount,
//   outOfNetwork: nextClaim.outOfNetwork,
//   overallMemberResponsibility: nextClaim.overallMemberResponsibility,
//   medishareResponsibility: nextClaim.medishareResponsibility,
//   coInsAmt: nextClaim.coInsAmt,
//   memberResponsibility: nextClaim.overallMemberResponsibility,
//   medishareResponsibility: nextClaim.medishareResponsibility,
//   serviceLines: nextClaim.serviceLines,
// });
